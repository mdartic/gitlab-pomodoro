module.exports = {
  root: true,
  env: {
    node: true,
  },
  'extends': [
    'plugin:vue/essential',
    '@vue/eslint-config-standard',
  ],
  rules: {
    'no-console': 'off',
    'no-debugger': 'off',
    'comma-dangle': ['error', 'always-multiline'],
    'vue/attribute-hyphenation': [
      'error',
      'always',
    ],
    'vue/html-indent': [
      'error',
      2,
    ],
    'vue/html-self-closing': 'error',
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
}
