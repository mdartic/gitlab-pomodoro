/**
 * Transform a hash part of a URL
 * http://you-url/your-route#hash
 * to a query form like
 * http://you-url/your-route?query
 *
 * @param {String} hash
 * Hash part of a URL
 */
export function hashToQuery (hash) {
  const query = {}
  const hashArray = hash.replace('#', '').split('&')
  hashArray.forEach(k => {
    const currentElement = k.split('=')
    query[currentElement[0]] = currentElement[1]
  })
  return query
}
