import API from '@/services/api'

const PROJECTS_NAMESPACE = 'projects'
const GET_PROJECTS = 'GET_PROJECTS'
const SET_PROJECTS = 'SET_PROJECTS'
const SELECT_PROJECT = 'SELECT_PROJECT'
const SET_SELECTED_PROJECT = 'SET_SELECTED_PROJECT'
const FETCH_ISSUES_SELECTED_PROJECT = 'FETCH_ISSUES_SELECTED_PROJECT'
const UPDATE_SELECTED_PROJECT = 'UPDATE_SELECTED_PROJECT'
const UPDATE_ISSUE = 'UPDATE_ISSUE'
const FETCH_PROJECTS = 'FETCH_PROJECTS'

export const GET_PROJECTS_NS = PROJECTS_NAMESPACE + '/' + GET_PROJECTS
export const SELECT_PROJECT_NS = PROJECTS_NAMESPACE + '/' + SELECT_PROJECT
export const UPDATE_ISSUE_NS = PROJECTS_NAMESPACE + '/' + UPDATE_ISSUE

export default {
  namespaced: true,
  state: {
    headers: [
      {
        text: 'Project id',
        align: 'left',
        value: 'id',
      },
      {
        text: 'Project Name',
        align: 'left',
        value: 'name',
      },
    ],
    items: [],
    total: 0,
    currentPage: 1,
    pages: 0,
    loading: false,
    selectedProject: null,
    labels: [],
  },
  mutations: {
    [FETCH_PROJECTS] (state) {
      state.loading = true
    },
    [SET_PROJECTS] (state, { pages, items }) {
      state.pages = pages
      state.items = items
      state.loading = false
    },
    [SET_SELECTED_PROJECT] (state, selectedProject) {
      state.selectedProject = selectedProject
    },
    [FETCH_ISSUES_SELECTED_PROJECT] (state) {
      state.selectedProject.loadingIssues = true
    },
    [UPDATE_SELECTED_PROJECT] (state, { milestones, issues, labels }) {
      state.selectedProject.loadingIssues = false
      state.selectedProject.milestones = milestones
      state.selectedProject.issues = issues
      state.selectedProject.labels = labels
    },
  },
  actions: {
    /**
     * get the projects from the gitlab instance
     * via the API and a search param if set
     */
    async [GET_PROJECTS] ({ commit, state }, search = '') {
      commit(FETCH_PROJECTS)
      let items = await API.getProjects({
        per_page: 15,
        page: state.currentPage,
        search,
      })
      commit(SET_PROJECTS, {
        pages: items.totalPages,
        items: items.data && items.data.map((currentProject) => {
          return {
            ...currentProject,
            issues: [],
            milestones: [],
            labels: [],
          }
        }),
      })
    },
    /**
     * select a project as the current project
     * fetch the issues / milestones of the current project
     */
    async [SELECT_PROJECT] ({ commit, state }, id) {
      commit(SET_SELECTED_PROJECT, state.items.find(v => v.id === id))
      commit(FETCH_ISSUES_SELECTED_PROJECT)
      const issues = await API.getIssues({
        projectId: id,
        withTimeStat: true,
        allInOne: true,
      })
      const milestones = await API.getMilestones({ projectId: id })
      const defaultState = { editable: false, updating: false }
      const labels = new Set()
      issues.forEach(i => {
        i.labels.forEach(l => labels.add(l))
      })
      commit(UPDATE_SELECTED_PROJECT, {
        milestones,
        issues: issues.map(i => ({ ...i, ...defaultState })),
        labels,
      })
    },
    /**
     * update the issue given in params
     * * milestone
     * * state
     * * time_stats
     */
    async [UPDATE_ISSUE] ({ commit, state }, issue) {
      let currentIssue = state.items.find(p => p.id === issue.project_id)
        .issues.find(i => i.iid === issue.iid)
      currentIssue.updating = true
      const resultUpdate = await API.updateIssue(issue)
      currentIssue.updating = false
      currentIssue.milestone = resultUpdate.milestone
      currentIssue.state = resultUpdate.state
      currentIssue.time_stats = resultUpdate.time_stats
      return resultUpdate
    },
  },
}
