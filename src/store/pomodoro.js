import { pushNotification } from '@/services/notifications'

const POMODORO_NAMESPACE = 'pomodoro'

// pomodoro status
export const POMODORO_STATUS_STOPPED = 'POMODORO_STATUS_STOPPED'
export const POMODORO_STATUS_RUNNING = 'POMODORO_STATUS_RUNNING'
export const POMODORO_STATUS_PAUSED = 'POMODORO_STATUS_PAUSED'
export const POMODORO_STATUS_FINISHED = 'POMODORO_STATUS_FINISHED'
export const POMODORO_STATUS_ERROR = 'POMODORO_STATUS_ERROR'

export const POMODORO_TYPE_FOCUS = 'POMODORO_TYPE_FOCUS'
export const POMODORO_TYPE_LEISURE = 'POMODORO_TYPE_LEISURE'

// dispatch actions
const POMODORO_START = 'POMODORO_START'
const POMODORO_RESTART = 'POMODORO_RESTART'
const POMODORO_STOP = 'POMODORO_STOP'
const POMODORO_PAUSE = 'POMODORO_PAUSE'
const POMODORO_SET_PERIOD = 'POMODORO_SET_PERIOD'
const POMODORO_TICK = 'POMODORO_TICK'
const POMODORO_RESET_RECORDS = 'POMODORO_RESET_RECORDS'

// dispatch actions namespaced
export const POMODORO_START_NS = `${POMODORO_NAMESPACE}/${POMODORO_START}`
export const POMODORO_RESTART_NS = `${POMODORO_NAMESPACE}/${POMODORO_RESTART}`
export const POMODORO_STOP_NS = `${POMODORO_NAMESPACE}/${POMODORO_STOP}`
export const POMODORO_PAUSE_NS = `${POMODORO_NAMESPACE}/${POMODORO_PAUSE}`
export const POMODORO_SET_PERIOD_NS = `${POMODORO_NAMESPACE}/${POMODORO_SET_PERIOD}`
export const POMODORO_RESET_RECORDS_NS = `${POMODORO_NAMESPACE}/${POMODORO_RESET_RECORDS}`

// getters namespaced
export const POMODORO_GETTERS_TIMER_IS_RUNNING = `${POMODORO_NAMESPACE}/timerIsRunning`
export const POMODORO_GETTERS_TIMER_IS_FINISHED = `${POMODORO_NAMESPACE}/timerIsFinished`
export const POMODORO_GETTERS_TIMER_IS_STOPPED = `${POMODORO_NAMESPACE}/timerIsStopped`
export const POMODORO_GETTERS_TIMER_IN_ERROR = `${POMODORO_NAMESPACE}/timerInError`
export const POMODORO_GETTERS_TIMER_IS_OF_TYPE_FOCUS = `${POMODORO_NAMESPACE}/timerIsOfTypeFocus`
export const POMODORO_GETTERS_TIMER_IS_OF_TYPE_LEISURE = `${POMODORO_NAMESPACE}/timerIsOfTypeLeisure`

// internal mutations
const SET_ISSUE = 'SET_ISSUE'
const SET_STATUS = 'SET_STATUS'
const SET_TIMEOUT_ID = 'SET_TIMEOUT_ID'
const SET_PERIOD = 'SET_PERIOD'
const INCREASE_TIME_SPENT = 'INCREASE_TIME_SPENT'
const RESET = 'RESET'
const RESET_RECORDS = 'RESET_RECORDS'
const TOGGLE_TYPE = 'TOGGLE_TYPE'

const defaultPeriodFocus = 25
const defaultPeriodLeisure = 5

/**
 * This pomodoro store helps the app to manage the pomodoro timer
 * * choose of a pomodoro period
 * * manage the timer state (running / pause / ...) and pomodoro state (focus / distract)
 * * manage the timeouts
 * * record the repartition of time spent on each issue ( = task )
 */
export default {
  namespaced: true,
  state: {
    periodFocus: defaultPeriodFocus,
    periodFocusInSecond: defaultPeriodFocus * 60,
    periodsFocusAvailables: [5, 10, 15, 25, 30, 35, 40, 45, 50, 55],
    periodLeisure: defaultPeriodLeisure,
    periodLeisureInSecond: defaultPeriodLeisure * 60,
    periodsLeisureAvailables: [5, 10, 15],
    spentTime: 0,
    progress: 0,
    remainingTime: '',
    timeoutId: null,
    status: POMODORO_STATUS_STOPPED,
    errorMessage: null,
    records: [],
    currentRecord: null,
    type: POMODORO_TYPE_FOCUS,
  },
  getters: {
    timerIsRunning (state) {
      return state.status === POMODORO_STATUS_RUNNING
    },
    timerIsFinished (state) {
      return state.status === POMODORO_STATUS_FINISHED
    },
    timerIsStopped (state) {
      return state.status === POMODORO_STATUS_STOPPED
    },
    timerInError (state) {
      return state.status === POMODORO_STATUS_ERROR
    },
    timerIsOfTypeFocus (state) {
      return state.type === POMODORO_TYPE_FOCUS
    },
    timerIsOfTypeLeisure (state) {
      return state.type === POMODORO_TYPE_LEISURE
    },
  },
  mutations: {
    [SET_ISSUE] (state, issue) {
      // first search if an issue doesn't already exist
      const indexRecord = state.records.findIndex(v => (v.issue.iid === issue.iid))
      if (indexRecord >= 0) {
        state.currentRecord = state.records[indexRecord]
      } else {
        state.records.push({ issue, spentTime: 0 })
        state.currentRecord = state.records[state.records.length - 1]
      }
    },
    [SET_STATUS] (state, { status, errorMessage }) {
      state.status = status
      state.errorMessage = errorMessage
    },
    [SET_TIMEOUT_ID] (state, timeoutId) {
      state.timeoutId = timeoutId
    },
    [SET_PERIOD] (state, period) {
      state.periodFocus = period
      state.periodFocusInSecond = period * 60
    },
    [INCREASE_TIME_SPENT] (state) {
      state.spentTime++
      state.errorMessage = null
      let periodInSecond = state.periodLeisureInSecond
      if (state.type === POMODORO_TYPE_FOCUS) {
        state.currentRecord.spentTime++
        periodInSecond = state.periodFocusInSecond
      }
      state.progress = state.spentTime / periodInSecond * 100
      let minutes = Math.trunc((periodInSecond - state.spentTime) / 60)
      let seconds = (periodInSecond - state.spentTime) % 60
      state.remainingTime = minutes + ':' + (seconds < 10 ? '0' + seconds : seconds)
      document.title = state.remainingTime
    },
    [RESET] (state, resetRecords = false) {
      state.spentTime = 0
      state.progress = 0
      state.errorMessage = null
      state.remainingTime = ''
      state.timeoutId = null
      state.status = POMODORO_STATUS_STOPPED
      if (resetRecords === true) {
        state.records = []
        state.currentRecord = null
      }
    },
    [TOGGLE_TYPE] (state) {
      if (state.type === POMODORO_TYPE_FOCUS) {
        state.type = POMODORO_TYPE_LEISURE
        state.periodLeisureInSecond = state.periodLeisure * 60
      } else {
        state.type = POMODORO_TYPE_FOCUS
        state.periodFocusInSecond = state.periodFocus * 60
      }
    },
    [RESET_RECORDS] (state) {
      state.records = []
    },
  },
  actions: {
    [POMODORO_START] ({ commit, dispatch }, issue) {
      // check issue is a good one
      if (issue && issue.iid && issue.time_stats) {
        commit(SET_ISSUE, issue)
      }
      dispatch(POMODORO_TICK)
    },
    [POMODORO_PAUSE] ({ commit, state }) {
      window.clearTimeout(state.timeoutId)
      commit(SET_STATUS, { status: POMODORO_STATUS_PAUSED })
      commit(SET_TIMEOUT_ID, null)
    },
    /**
     * Stop the pomodoro timer, reset the current state,
     * change the pomodoro type if we were in focus mode
     */
    [POMODORO_STOP] ({ commit, state, getters }) {
      window.clearTimeout(state.timeoutId)
      commit(RESET, true)
      if (getters.timerIsOfTypeLeisure) {
        commit(TOGGLE_TYPE)
      }
    },
    [POMODORO_SET_PERIOD] ({ commit }, period) {
      commit(SET_PERIOD, period)
    },
    [POMODORO_TICK] ({ commit, dispatch, state, getters }) {
      console.log('tick')
      if (getters.timerIsFinished) {
        commit(RESET)
        commit(TOGGLE_TYPE)
      }
      // we need a current record if we are in focus mode
      if (getters.timerIsOfTypeFocus && state.currentRecord === null) {
        commit(SET_STATUS, {
          status: POMODORO_STATUS_ERROR,
          errorMessage: 'No issue selected for timer. Please choose one first.',
        })
      } else {
        commit(SET_STATUS, { status: POMODORO_STATUS_RUNNING })
        const timeoutId = window.setTimeout(() => {
          commit(INCREASE_TIME_SPENT)
          console.log('end of period ?', state.spentTime === state.periodFocusInSecond)
          if (state.spentTime === state.periodFocusInSecond) {
            console.log('notification ! ')
            commit(SET_STATUS, { status: POMODORO_STATUS_FINISHED })
            pushNotification('Pomodoro finish !', `
            The timer is over (${state.period} mn) !
            Click here to check your status timer.
            `)
          } else {
            console.log('another tick...')
            dispatch(POMODORO_TICK)
          }
        }, 1000)
        commit(SET_TIMEOUT_ID, timeoutId)
      }
    },
    [POMODORO_RESET_RECORDS] ({ commit }) {
      commit(RESET_RECORDS)
    },
    [POMODORO_RESTART] ({ commit, getters, dispatch }) {
      commit(RESET)
      if (getters.timerIsOfTypeLeisure) {
        commit(TOGGLE_TYPE)
      }
      dispatch(POMODORO_START)
    },
  },
}
