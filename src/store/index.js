import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import projects from './projects'
import settings from './settings'
import pomodoro from './pomodoro'

Vue.use(Vuex)

export default new Vuex.Store({
  // strict: true,
  modules: {
    projects,
    settings,
    pomodoro,
  },
  plugins: [createPersistedState({
    key: 'gitlab-pomodoro-store',
    paths: [
      'settings',
      'pomodoro',
    ],
  })],
})
