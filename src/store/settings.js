import api from '@/services/api'

export const SETTINGS_NAMESPACE = 'settings'
const SET_TOKEN = 'SET_TOKEN'
const SET_GITLAB_CURRENT_INSTANCE = 'SET_GITLAB_CURRENT_INSTANCE'
const GITLAB_MAKINA_INSTANCE = 'Makina Corpus'
const GITLAB_COM_INSTANCE = 'Gitlab.com'

export const SET_TOKEN_NS = SETTINGS_NAMESPACE + '/' + SET_TOKEN
export const SET_GITLAB_CURRENT_INSTANCE_NS = SETTINGS_NAMESPACE + '/' + SET_GITLAB_CURRENT_INSTANCE

// url https://gitlab.makina-corpus.com/oauth/authorize?client_id=&redirect_uri=http://localhost:8080/registertoken&response_type=token&state=YOUR_UNIQUE_STATE_HASH

/**
 * Settings store for VueX
 * * access token for gitlab instance
 * * url of gitlab instance
 */
export default {
  namespaced: true,
  state: {
    instances: {
      [GITLAB_MAKINA_INSTANCE]: {
        appId: 'dccdb4827948a1931e1879652d799ba4ab3e43c1db2ff4e826b7448d16b55740',
        token: null,
        url: 'https://gitlab.makina-corpus.net',
        api: 'https://gitlab.makina-corpus.net/api/v4/',
        hash: 'pouet pouet pouet',
      },
      [GITLAB_COM_INSTANCE]: {
        appId: '20186606e1ec5911b51aa72917816e51e85d5be3d830aee2dd25a5ed5d015907',
        token: null,
        url: 'https://gitlab.com',
        api: 'https://gitlab.com/api/v4/',
        hash: 'pouic pouic pouic',
      },
    },
    currentInstanceId: GITLAB_MAKINA_INSTANCE,
    availableInstances: [
      GITLAB_COM_INSTANCE,
      GITLAB_MAKINA_INSTANCE,
    ],
  },
  getters: {
    currentInstance (state) {
      return state.instances[state.currentInstanceId]
    },
  },
  mutations: {
    [SET_TOKEN] (state, { token, instanceId }) {
      state.instances[instanceId].token = token
    },
    [SET_GITLAB_CURRENT_INSTANCE] (state, instanceId) {
      state.currentInstanceId = instanceId
    },
  },
  actions: {
    [SET_TOKEN] ({ commit }, { token, instanceId }) {
      commit(SET_TOKEN, { token, instanceId })
    },
    [SET_GITLAB_CURRENT_INSTANCE] ({ commit, getters }, instanceId) {
      commit(SET_GITLAB_CURRENT_INSTANCE, instanceId)
      api.init(getters.currentInstance.token, getters.currentInstance.api)
    },
  },
}
