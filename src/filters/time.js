import Vue from 'vue'

Vue.filter('time', function (value) {
  if (!value) return ''
  return `${value} s /
          ${Math.trunc(value / 60)} mn /
          ${Math.trunc(value / 3600)} h`
})
