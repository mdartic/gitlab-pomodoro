/* global BASE_URL */

import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home'
import Timer from '@/views/Timer'
import Projects from '@/views/Projects'
import Settings from '@/views/Settings'
import { hashToQuery } from '@/utils/hashToQuery'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  base: BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/timer',
      name: 'Timer',
      component: Timer,
    },
    {
      path: '/projects',
      name: 'Projects',
      component: Projects,
    },
    {
      path: '/token-redirect',
      name: 'oauth',
      redirect: to => {
        // redirect gitlab auth request with hash
        // to the Settings route with a query instead of hash
        return {
          path: '/settings',
          hash: '',
          query: hashToQuery(to.hash),
        }
      },
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings,
    },
  ],
})
