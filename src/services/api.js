/* eslint camelcase: 0 */
import request from 'superagent'

const PROJECT = 'projects'
const ISSUES = 'issues'
const MILESTONES = 'milestones'

const HEADER_TOTAL_ITEMS = 'x-total'
const HEADER_PAGE = 'x-page'
const HEADER_TOTAL_PAGES = 'x-total-pages'

// const getNumberOfRessources = async (ressourcePath, filters = {}) => {
//   const filter = '?' + Object.keys(filters).map(function (c) { return c + '=' + filters[c]; }).join('&');
//   const numberOfItems = await request.head(_url + ressourcePath + filter)
//     .set({
//       'PRIVATE-TOKEN': _token,
//       'Accept': 'application/json'
//     })
//   return numberOfItems.res.headers['x-total'];
// }

class API {
  _token = null
  _url = null

  /**
   * Init the API with token & url
   * These params are used to send Authorization header
   * to the right Gitlab URL
   *
   * @param {String} token
   * Token to be used for Authorization header
   *
   * @param {String} url
   * Gitlab URL to connect
   */
  init (token, url) {
    this._token = token
    this._url = url
  }

  async _getRessources (ressourcePath, filters) {
    if (this._url === null || this._token === null) {
      throw new Error('API is not yet configured properly. Please set token & url first.')
    }
    // const numberOfItems = await getNumberOfRessources(ressourcePath, filters);
    const requestReturn = await request.get(this._url + ressourcePath)
      .query(Object.assign({}, /* { 'per_page': numberOfItems }, */ filters))
      .set({
        Authorization: 'Bearer ' + this._token,
        Accept: 'application/json',
      })
    return {
      data: JSON.parse(requestReturn.text),
      totalItems: parseInt(requestReturn.header[HEADER_TOTAL_ITEMS]),
      totalPages: parseInt(requestReturn.header[HEADER_TOTAL_PAGES]),
      page: parseInt(requestReturn.header[HEADER_PAGE]),
    }
  }

  async getProjects (parameters) {
    try {
      const projects = await this._getRessources(PROJECT, {
        simple: true,
        membership: true,
        ...parameters,
      })
      return projects
    } catch (error) {
      console.error(error)
      return []
    }
  }

  async getIssues ({
    projectId, filter, withTimeStat, allInOne,
  }) {
    try {
      const pathIssues = `${PROJECT}/${projectId
      }/${ISSUES}${filter ? `?${filter}` : ''}`
      const firstIssues = await this._getRessources(pathIssues, { sort: 'asc', per_page: 100 })
      let issues = firstIssues.data
      // if allInOne is true, we have to iterate over the number of pages
      if (allInOne === true && firstIssues.totalPages > 1) {
        for (let i = 2; i <= firstIssues.totalPages; i++) {
          const followingIssues = await this._getRessources(pathIssues, { sort: 'asc', per_page: 100, page: i })
          issues = issues.concat(followingIssues.data)
        }
      }
      return issues.map(i => ({
        projectId,
        ...i,
        milestone: i.milestone || {},
      }))
    } catch (error) {
      console.error(error)
      return []
    }
  }

  async getMilestones ({ projectId }) {
    try {
      const pathMilestones = `${PROJECT}/${projectId}/${MILESTONES}`
      const milestones = await this._getRessources(pathMilestones, { sort: 'asc', per_page: 100 })
      return milestones.data
    } catch (error) {
      console.error(error)
      return []
    }
  }

  /**
   * Update an issue on Gitlab API
   * - milestone
   * - state_event
   * - spent time
   * @returns new issue with updated data
   */
  async updateIssue ({
    project_id, iid, milestone_id, state_event, time_estimate, spent_time,
  }) {
    // https://docs.gitlab.com/ce/api/issues.html#edit-issue
    const requestReturnIssue = await request.put(`${this._url}/projects/${project_id}/issues/${iid}`)
      .query({ milestone_id, state_event })
      .set({
        Authorization: 'Bearer ' + this._token,
        Accept: 'application/json',
      })
    const newIssue = JSON.parse(requestReturnIssue.text)

    // https://docs.gitlab.com/ce/api/issues.html#add-spent-time-for-an-issue
    if (spent_time) {
      const requestReturnTimeSpent = await request.post(`${this._url}/projects/${project_id}/issues/${iid}/add_spent_time`)
        .query({ duration: spent_time })
        .set({
          Authorization: 'Bearer ' + this._token,
          Accept: 'application/json',
        })
      newIssue.time_stats = JSON.parse(requestReturnTimeSpent.text)
    }

    // https://docs.gitlab.com/ce/api/issues.html#set-a-time-estimate-for-an-issue
    if (time_estimate) {
      const requestReturnTimeEstimate = await request.post(`${this._url}/projects/${project_id}/issues/${iid}/time_estimate`)
        .query({ duration: time_estimate })
        .set({
          Authorization: 'Bearer ' + this._token,
          Accept: 'application/json',
        })
      newIssue.time_stats = JSON.parse(requestReturnTimeEstimate.text)
    }

    return newIssue
  }

  async addTimeToIssue ({ project_id, iid, spent_time }) {
    // const requestReturnTimeSpent =
    await request.post(`${this._url}projects/${project_id}/issues/${iid}/add_spent_time`)
      .query({ duration: spent_time })
      .set({
        Authorization: 'Bearer ' + this._token,
        Accept: 'application/json',
      })
    // newIssue.time_stats = JSON.parse(requestReturnTimeSpent.text)
  }
}

export default new API()
