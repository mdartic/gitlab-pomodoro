const REDIRECT_URL = process.env.NODE_ENV === 'production'
  ? 'https://mdartic.gitlab.io/gitlab-pomodoro/token-redirect'
  // ? 'http://madz-gitlab-pomodoro.surge.sh/token-redirect'
  : 'http://localhost:8080/token-redirect'
/**
 * Make the url to access for getting OAuth Token
 * This token will be used to access projects/issues
 * on the gitlab instance
 *
 * @param {String} gitlabUrl
 * URL of Gitlab instance
 *
 * @param {String} appId
 * Token identifying Gitlab Pomodoro in the gitlab instance
 *
 * @param {String} stateHash
 * Unique hash identifying the current user
 */
export function getUrlGitlabOAuth (gitlabUrl, appId, stateHash) {
  return `${gitlabUrl}/oauth/authorize?client_id=${appId}&redirect_uri=${REDIRECT_URL}&response_type=token&state=${stateHash}`
}

export default {
  getUrlGitlabOAuth,
}
