import { saveAs } from 'file-saver'
import API from '@/services/api'

export const exportProjectIssuesWithTimeData = async function (projectId) {
  // this.$set(state.generatingExport, projectId, true)
  console.log(`Exporting time stats for issues on project ${projectId}...\nplease wait...`)

  const timeStatsIssues = await API.getIssues({
    projectId: projectId,
    withTimeStat: true,
    allInOne: true,
  })
  const dataToWrite = ['iid;title;state;labels;milestone;time_estimate;total_time_spent;url;\n']
  timeStatsIssues.forEach(currentIssue => {
    dataToWrite.push(
      currentIssue.iid + ';' +
      currentIssue.title + ';' +
      currentIssue.state + ';' +
      currentIssue.labels + ';' +
      currentIssue.milestone.title + ';' +
      Math.round(currentIssue.time_stats.time_estimate / 360) / 10 + ';' +
      Math.round(currentIssue.time_stats.total_time_spent / 360) / 10 + ';' +
      currentIssue.web_url + '\n'
    )
  })
  var blob = new Blob(dataToWrite, { type: 'text/plain;charset=utf-8' })
  saveAs(blob, `export_${projectId}.csv`)
  // this.$set(state.generatingExport, projectId, false)
}
