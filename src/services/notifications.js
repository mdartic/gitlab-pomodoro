import Push from 'push.js'

export function pushNotification (message, body) {
  Push.create(message, {
    body,
    onClick: function () {
      window.focus()
      this.close()
    },
  })
}
