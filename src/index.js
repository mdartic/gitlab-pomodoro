import API from './services/api'
import CONFIG from './config/config'

const fs = require('fs')

const exportTimeStatsIssues = async () => {
  console.log(`Exporting time stats for issues on project ${CONFIG.projectId}...\nplease wait...`)

  API.init(CONFIG)

  const timeStatsIssues = await API.getIssues(CONFIG.projectId, '', true)

  const dataToWrite =
    `projectId;iid;title;state;labels;timeSpent(hours);url;\n${
      timeStatsIssues.map((currentIssue) => {
        const {
          projectId, iid, title, state, labels, timeSpent, url,
        } = currentIssue
        return `${projectId}${iid};${title};${state};${labels};${timeSpent};${url}`
      }).join('\n')}`

  fs.writeFile(`time-stats-issues-${CONFIG.projectId}.csv`, dataToWrite, 'utf8', (err) => {
    if (err) {
      console.error('Some error occured - file either not saved or corrupted file saved.')
    } else {
      console.log('It\'s saved!')
    }
  })
}

exportTimeStatsIssues()
