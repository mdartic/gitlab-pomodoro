// this file is an example of what is expected in CONFIG
// token is the token you have to create in your gitlab > Settings > Access Tokens
// see directly https://gitlab.com/profile/personal_access_tokens
// url is simply the url of your gitlab server API (v4 supported)
// please copy/paste this file in a 'config.js' file, in the same folder

export default {
  token: 'the-token-generated-on-your-gitlab',
  url: 'https://yourgitlab.com/api/v4/',
  projectId: 12345, // it's a number, don't use 'quotes'
}
